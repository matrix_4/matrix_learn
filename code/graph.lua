--生成随机零阶矩阵
function Print_Matrix(n, matrix)
    for i = 1,n do
        local s=""
        for j = 1, n do
          s = s..matrix[i][j].." "
        end
        print(s)
    end
end

function Generate_Random(n)
   local socket = require "socket"
   Matrix = {}
   for i = 1,n do
      local row = {} 
      for j = i+1,n do  
        math.randomseed(tostring(socket.gettime()):reverse():sub(1, 9))
        local m = math.random()
        row[j] = (m>=0.5 and 1) or 0
      end 
      row[i] = 0
      for j = 1, i-1 do
          row[j] = Matrix[j][i]
      end
      Matrix[i] = row
   end
   Print_Matrix(n, Matrix)
   return Matrix
end

function Multiple(n, Matrix)
    C = {}
    for i = 1,n do
        local row = {}
        for j = 1,n do
            local sum = 0
            for k = 1, n do
                sum= sum + Matrix[i][k]*Matrix[k][j]
            end
            row[j]  = sum
        end
        C[i] = row
    end
    return C
end

--判断是否为空
function Table_is_empty(a)
    return next(a)==nil
end

--查找路径
function Find_Road(Matrix, n, i, j, n1)
--建立邻接表
   local Paths = {}
   Path = {}
   Road  = {}
   local number = 1
  for k1 = 1,n do
      local row = {}
      local m = 1
      for k2 = 1, n do
        if Matrix[k1][k2]==1 then  
          row[m] = k2
          m = m + 1
        end
      end
      Road[k1] = row
  end
  Roads = {}
  Neighbors = {}
  Roads[1] = i
  --Neighbors = Road[i] --将起点加入
 -- table.insert(Roads,Neighbors[#Neighbors])--Roads加入栈顶
  while (not Table_is_empty(Roads)) do
    Node  = Roads[#Roads] --获取顶端节点
    for len  = 1, #Road[Node] do
      for len1 = 1,#Roads do    --比较与Roads里是否有相同的元素，把相同的去掉
        if Road[Node][len] == Roads[len1] then
          table.remove(Road[Node],len)
        end
      end
    end
    if (not Table_is_empty(Road[Node]) )  then--如果邻接不为0   
      table.insert(Roads,Road[Node][#Road[Node]])--加入Roads栈顶
      table.remove(Road[Node])--将顶部移除 
      if (not Table_is_empty(Road[Node]) )  then
         for number2 = 1, #Road[Node] do
           table.insert(Neighbors,Road[Node][number2])  --将邻点加入
         end
      end
    else if (Table_is_empty(Road[Node])) and (Roads[#Roads]~=j) then
      table.remove(Roads)
    end
    if Roads[#Roads]==j then  
        Paths = Roads
        string = ''
       if #Paths==n1 + 1 then
          for i = 1, n1 do
            string = string..Paths[i] ..'->'
          end
            string = string.. Paths[n1+1]
            print(string)
        end
        number = number+1
        table.remove(Roads)
    end
  end
 end
  
end
print("please input the dimension:")
N = tonumber(io.read())
Matrix = Generate_Random(N)
print("please input i, j, n:")
I = tonumber(io.read())
J = tonumber(io.read())
N1 = tonumber(io.read())
Matrix1 = Matrix
for i = 1, N1-1 do
   Matrix1 =  Multiple(N,Matrix1)
end
print("The number of the road is: ", Matrix1[I][J])
Find_Road(Matrix, N, I, J, N1)